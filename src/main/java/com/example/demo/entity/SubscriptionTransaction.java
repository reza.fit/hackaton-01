package com.example.demo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "subscription_transaction")
public class SubscriptionTransaction {

    @Id
    private Integer stId;

    private String msisdn;

    private int dataIn;

    private int dataOut;

    private LocalDateTime trxStart;

    private LocalDateTime trxEnd;

}
