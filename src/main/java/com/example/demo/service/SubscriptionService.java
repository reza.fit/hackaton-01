package com.example.demo.service;

import com.example.demo.entity.SubscriptionTransaction;
import com.example.demo.repository.SubscriberTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubscriptionService {

    private final SubscriberTransactionRepository repository;

    @Autowired
    public SubscriptionService(SubscriberTransactionRepository repository){
        this.repository = repository;
    }

    public List<SubscriptionTransaction> getAllTransaction(){
        return repository.findAll();
    }

    public Optional<SubscriptionTransaction> getByMsisdn(String msisdn){
        return repository.findByMsisdn(msisdn);
    }


}
