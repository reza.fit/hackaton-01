package com.example.demo.controller;

import com.example.demo.dto.ResponseDto;
import com.example.demo.entity.SubscriptionTransaction;
import com.example.demo.service.SubscriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/v1/subscription-trx")
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    @Autowired
    public SubscriptionController(SubscriptionService subscriptionService){
        this.subscriptionService = subscriptionService;
    }

    @GetMapping
    public List<SubscriptionTransaction> getAllTransaction(){
        return subscriptionService.getAllTransaction();
    }

    @PostMapping("/getByMsisdn/{msisdn}")
    public ResponseEntity<ResponseDto<SubscriptionTransaction>>  getByMsisdn(@PathVariable String msisdn){
        Optional<SubscriptionTransaction> subscription = subscriptionService.getByMsisdn(msisdn);
        if(subscription.isPresent()){
            ResponseDto<SubscriptionTransaction> response = new ResponseDto<>(
                    "ok", "00", "success", subscription.get());
            return ResponseEntity.ok(response);
        }else{
            ResponseDto<SubscriptionTransaction> response = new ResponseDto<>(
                    "error", "01", "Subscriber not found", null);
            return ResponseEntity.status(404).body(response);
        }
    }
}
