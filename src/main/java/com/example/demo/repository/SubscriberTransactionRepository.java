package com.example.demo.repository;

import com.example.demo.entity.SubscriptionTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubscriberTransactionRepository extends JpaRepository<SubscriptionTransaction, Integer> {

    Optional<SubscriptionTransaction> findByMsisdn(String msisdn);
}
