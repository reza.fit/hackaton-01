package com.example.demo;

import com.example.demo.entity.SubscriptionTransaction;
import com.example.demo.repository.SubscriberTransactionRepository;
import com.example.demo.service.SubscriptionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class SubscriptionTransactionServiceTest {

    @Autowired
    private SubscriptionService service;

    @MockBean
    private SubscriberTransactionRepository repository;

    @Test
    public void testGetAllTransaction(){
        SubscriptionTransaction transaction1 = new SubscriptionTransaction();
        transaction1.setStId(1);
        transaction1.setMsisdn("1234567890");

        SubscriptionTransaction transaction2 = new SubscriptionTransaction();
        transaction2.setStId(2);
        transaction2.setMsisdn("0987654321");

        List<SubscriptionTransaction> transactions = Arrays.asList(transaction1, transaction2);

        when(repository.findAll()).thenReturn(transactions);

        List<SubscriptionTransaction> result = service.getAllTransaction();
        assertEquals(2, result.size());
        assertEquals("1234567890", result.get(0).getMsisdn());
        assertEquals("0987654321", result.get(1).getMsisdn());
    }
}
